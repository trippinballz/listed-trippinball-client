
// Validation functions

function validateCompanyModal(company) {
    if (isEmpty(company.name)) {
        showError("Nimi on kohustuslik!")
        return false;
    }
    if (!isEmpty(company.employees) && !isInteger(company.employees)) {
        showError("Töötajate arv peab olema täisarv!");
        return false;
    }
    if (!isEmpty(company.revenue) && !isDecimal(company.revenue)) {
        showError("Müügitulu peab olema number!");
        return false;
    }
    if (!isEmpty(company.netIncome) && !isDecimal(company.netIncome)) {
        showError("Netotulu peab olema number!");
        return false;
    }
    if (!isEmpty(company.securities) && !isInteger(company.securities)) {
        showError("Aktsiate arv peab olema täisarv!");
        return false;
    }
    if (!isEmpty(company.securityPrice) && !isDecimal(company.securityPrice)) {
        showError("Aktsiahind peab olema number!");
        return false;
    }
    if (!isEmpty(company.dividends) && !isDecimal(company.dividends)) {
        showError("Dididend peab olema number!");
        return false;
    }
    return true;
}

function validateCredentials(credentials) {
    return !isEmpty(credentials.username) && !isEmpty(credentials.password);
}

function isInteger(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+$/;
    return regex.test(text);
}

function isDecimal(text) {
    if (text == null) {
        return false;
    }
    let regex = /^[\-]?\d+(\.\d+)?$/;
    return regex.test(text);
}

function isEmpty(text) {
    return (!text || 0 === text.length);
}

// DOM retrieval functions

function getFileInput() {
    return document.getElementById("file");
}

function getTripFromModal() {
    return {
        "tripID": document.getElementById("tripID").value,
        "tripLocation": document.getElementById("tripLocation").value,
        "tripType": document.getElementById("tripType").value,
        "tripStartTime": document.getElementById("tripStartTime").value,
        "tripEndTime": document.getElementById("tripEndTime").value,
    };
}

function getAccomodationFromModal() {
    return {

        "tripID": document.getElementById("accomodationTripId").value,
        "id": document.getElementById("accomodationId").value,
        "accomodationName": document.getElementById("accomodationModalName").value,
        "accomodationStartTime": document.getElementById("accomodationModalStartTime").value,
        "accomodationEndTime": document.getElementById("accomodationModalEndTime").value,
        "accomodationAddress": document.getElementById("accomodationModalAddress").value,
        "accomodationPhoneNo": document.getElementById("accomodationModalPhoneNo").value,
        "accomodationType": document.getElementById("accomodationModalType").value,
        "accomodationExtraInfo": document.getElementById("accomodationModalExtraInfo").value
    };
}

function getTransportFromModal() {
    return {

        "tripID": document.querySelector("#transportTripId").value,
        "id": document.querySelector("#transportId").value,
        "transportProvider": document.getElementById("transportModalProvider").value,
        "transportStartTime": document.getElementById("transportModalStartTime").value,
        "transportEndTime": document.getElementById("transportModalEndTime").value,
        "transportStartLocation": document.getElementById("transportModalStartLocation").value,
        "transportEndLocation": document.getElementById("transportModalEndLocation").value,
        "transportType": document.getElementById("transportModalType").value
    };
}

function getChecklistFromModal() {
    return {
        "tripID": document.querySelector("#checklistTripId").value,
        "id": document.querySelector("#checklistId").value,
        "listItem": document.getElementById("checklistModalItem").value
    };
}

function getTaskFromModal() {
    return {
        "tripID": document.querySelector("#taskTripId").value,
        "id": document.querySelector("#taskId").value,
        "taskItem": document.getElementById("taskModalItem").value
    };
}

function getTravelDocumentFromModal() {
    return {
        "tripID": document.querySelector("#travelDocumentTripId").value,
        "id": document.querySelector("#travelDocumentId").value,
        "travelDocumentName": document.getElementById("travelDocumentModalName").value,
        "travelDocumentStartTime": document.getElementById("travelDocumentModalStartTime").value,
        "travelDocumentEndTime": document.getElementById("travelDocumentModalEndTime").value,
        "travelDocumentExtraInfo": document.getElementById("travelDocumentModalExtraInfo").value
    };
}


function getCredentialsFromLoginContainer() {
    return {
        username: document.getElementById("username").value,
        password: document.getElementById("password").value
    };
}

function getCredentialsFromRegisterContainer() {
    return {
        username: document.getElementById("registerUsername").value,
        password: document.getElementById("registerPassword").value
    };
}

// Financial functions

function formatNumber(num) {
    if (!isDecimal(num)) {
        return 0;
    }
    num = Math.round(num * 100) / 100;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// Chart Functions

function generateRandomColor() {
    let r = Math.floor(Math.random() * 255);
    let g = Math.floor(Math.random() * 255);
    let b = Math.floor(Math.random() * 255);
    return `rgb(${r},${g},${b})`;
}

function composeChartDataset(companies) {
    if (companies != null && companies.length > 0) {
        let data = companies.map(company => Math.round(company.marketCapitalization));
        let backgroundColors = companies.map(generateRandomColor);
        return [{
            data: data,
            backgroundColor: backgroundColors,
        }];
    }
    return [];
}

function composeChartLabels(companies) {
    return companies != null && companies.length > 0 ? companies.map(company => company.name) : [];
}

function composeChartData(companies) {
    return {
        datasets: composeChartDataset(companies),
        labels: composeChartLabels(companies)
    };
}
