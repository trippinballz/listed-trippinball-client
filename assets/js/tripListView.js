function generateTripList(trips) {
    console.log(trips);
    let rows = "";
    for (let i = 0; i < trips.length; i++) {


        rows = `
        <div class="row" id="tripHeader">
        <div>
        
        <div class="col-lg-12" id="tripTypeHeading1" style="font-size: 32px; font-weight: 550; color: white;">
        ${trips[i].tripType}
        </div>
        <div class="col-lg-12" id="tripTypeHeading2" style="font-size: 20px; padding-top: 15px; color: white;">
       ${trips[i].tripLocation}, from ${trips[i].tripStartTime} to ${trips[i].tripEndTime} ${generateTripRowButtonEdit(trips[i])}${generateTripRowButtonDelete(trips[i])}
        </div>
        </div>
    </div>

            <div class="row">
            <div class="col-12" id="informationBox" style="display: flex">
                <div class="list-group" id="list-tab" role="tablist" style="margin-right: 5px">
                    <a class="list-group-item list-group-item-action active" id="list-home-list-${trips[i].tripID}" data-toggle="list" href="#list-home-${trips[i].tripID}" style="font-size: 14px;" role="tab" aria-controls="home"><i class="material-icons" style="font-size: 36px; align-items-center;">hotel</i> <span class="hidden-in-mobile">Accommodation</span></a>
                    <a class="list-group-item list-group-item-action" id="list-profile-list-${trips[i].tripID}" data-toggle="list" href="#list-profile-${trips[i].tripID}" style="font-size: 14px;" role="tab" aria-controls="profile"><i class="material-icons" style="font-size: 36px;">train</i> <span class="hidden-in-mobile">Transport</span></a>
                    <a class="list-group-item list-group-item-action" id="list-messages-list-${trips[i].tripID}" data-toggle="list" href="#list-messages-${trips[i].tripID}" style="font-size: 14px;" role="tab" aria-controls="messages"><i class="material-icons" style="font-size: 36px;">done</i>  <span class="hidden-in-mobile">Checklist</span></a>
                    <a class="list-group-item list-group-item-action" id="list-settings-list-${trips[i].tripID}" data-toggle="list" href="#list-settings-${trips[i].tripID}" style="font-size: 14px;" role="tab" aria-controls="settings"><i class="material-icons" style="font-size: 36px;">format_list_bulleted</i> <span class="hidden-in-mobile">Tasks</span></a>
                    <a class="list-group-item list-group-item-action" id="list-travelDocuments-list-${trips[i].tripID}" data-toggle="list" href="#list-travelDocuments-${trips[i].tripID}" style="font-size: 14px;" role="tab" aria-controls="travelDocuments"><i class="material-icons" style="font-size: 36px;">book</i> <span class="hidden-in-mobile">Travel Documents</span></a>
                </div>
                <div class="tab-content" id="nav-tabContent">
                    <div class="row" style="align-items: right;"></div>
                    
                    
                        <div class="tab-pane fade show active" style="font-size: 24px;"id="list-home-${trips[i].tripID}" role="tabpanel" aria-labelledby="list-home-list-
                        ${trips[i].tripID}">
                        <button type="button" class="btn btn-outline-success" onClick="openAccomodationModalForInsert(${trips[i].tripID})"><i class="material-icons" style="font-size: 32px;">add_circle_outline</i></button>
                        <div id="accomodationPanel-${trips[i].tripID}">${generateAccomodations(trips[i].accomodations)}</div>
                        </div>
                        <div class="tab-pane fade" style="font-size: 24px;" id="list-profile-${trips[i].tripID}" role="tabpanel" aria-labelledby="list-profile-list-
                        ${trips[i].tripID}">
                        <button type="button" class="btn btn-outline-success" onClick="openTransportModalForInsert(${trips[i].tripID})"><i class="material-icons" style="font-size: 32px;">add_circle_outline</i></button>    
                        <div id="transportPanel-${trips[i].tripID}">${generateTransport(trips[i].transport)}</div>
                        </div>
                        <div class="tab-pane fade" style="font-size: 24px;" id="list-messages-${trips[i].tripID}" role="tabpanel" aria-labelledby="list-messages-list-
                        ${trips[i].tripID}">
                        <button type="button" class="btn btn-outline-success" onClick="openChecklistModalForInsert(${trips[i].tripID})"><i class="material-icons" style="font-size: 32px;">add_circle_outline</i></button>
                        <div id="checklistPanel-${trips[i].tripID}">${generateChecklists(trips[i].checklists)}</div>
                        </div>
                        <div class="tab-pane fade" style="font-size:24px;" id="list-settings-${trips[i].tripID}" role="tabpanel" aria-labelledby="list-settings-list-
                        ${trips[i].tripID}">
                        <button type="button" class="btn btn-outline-success" onClick="openTaskModalForInsert(${trips[i].tripID})"><i class="material-icons" style="font-size: 32px;">add_circle_outline</i></button>
                        <div id="taskPanel-${trips[i].tripID}">${generateTasks(trips[i].tasks)}</div>
                        </div>
                        <div class="tab-pane fade" style="font-size: 24px;" id="list-travelDocuments-${trips[i].tripID}" role="tabpanel" aria-labelledby="list-travelDocuments-list-
                        ${trips[i].tripID}">
                        <button type="button" class="btn btn-outline-success" onClick="openTravelDocumentModalForInsert(${trips[i].tripID})"><i class="material-icons" style="font-size: 32px;">add_circle_outline</i></button>
                        <div id="travelDocumentPanel-${trips[i].tripID}">${generateTravelDocuments(trips[i].travelDocuments)}</div>
                        </br>
                        </div>
                    </div>
                </div>
          </div>


        ` + rows;

        // rows += `
        // ${generateTripRowHeading(trips[i]) }


        //     ${generateTripRowElement(

        //     'Location:', trips[i].tripLocation != null ? trips[i].tripLocation : 'info puudub',
        //     'Start Time:', trips[i].tripStartTime != null ? trips[i].tripStartTime : 'info puudub',
        //     'End Time:', trips[i].tripEndTime != null ? trips[i].tripEndTime : 'info puudub',
        // )}
        //     ${generateTripRowButtons(trips[i])}

        // `;
    }




    document.getElementById("tripList").innerHTML = /*html*/`
         <div class="row justify-content-center">
            <div class="col-lg-10">
                <div class="col-12">
                    ${rows}
                </div>
            </div>
        </div>
    `;
}

function generateAccomodations(accomodations) {
    let accomodationsHtml = "";

    for (let i = 0; i < accomodations.length; i++) {
        accomodationsHtml = accomodationsHtml + `
       
        
            <div id="accomodationName">${accomodations[i].accomodationName} 
            
            <button  class="btn btn-outline-secondary" onClick="openAccomodationModalForUpdate(${accomodations[i].id})">
            <i class="material-icons">
            edit
            </i></button>
            <button class="btn btn-outline-danger" onClick="removeAccomodation(${accomodations[i].id})"><i class="material-icons">
            clear
            </i></button>
            <button  class="btn btn-outline-secondary"><i class="material-icons" style="font-size: 24px; vertical-align: -20%;">
            attach_file
            </i></button>
            </div>
            <div class="row">
           <div class="col-6" id="accomodationAddress"> From ${accomodations[i].accomodationStartTime}
            until ${accomodations[i].accomodationEndTime}</br>
            ${accomodations[i].accomodationAddress}</br>
            ${accomodations[i].accomodationPhoneNo}</br>
            ${accomodations[i].accomodationType}</div>

            <div class="col-6" id="accomodationExtraInfo">${accomodations[i].accomodationExtraInfo}</div></div></br>
        `;
    }
    console.log(accomodationsHtml);
    return accomodationsHtml;
}

function generateTransport(transport) {
    let transportHtml = "";

    for (let i = 0; i < transport.length; i++) {
        transportHtml = transportHtml + `

        
        <div id="transportheader">${transport[i].transportProvider}
        
        <button  class="btn btn-outline-secondary" onClick="openTransportModalForUpdate(${transport[i].id})">
        <i class="material-icons">
        edit
        </i></button>
        <button class="btn btn-outline-danger" onClick="removeTransport(${transport[i].id}, ${transport[i].tripID})"><i class="material-icons">
        clear
        </i></button>
        <button  class="btn btn-outline-secondary"><i class="material-icons" style="font-size: 24px; vertical-align: -20%;">
        attach_file
        </i></button>
        </div>
        
        <div class="row">    
        <div  class="col-12" id="transportlocation">${transport[i].transportStartLocation} to
         ${transport[i].transportEndLocation} from ${transport[i].transportStartTime} until ${transport[i].transportEndTime}</div>
 </div><div id="transportinformation">${transport[i].transportType}</div></br>
           
           
           
        `;
    }
    console.log(transportHtml);
    return transportHtml;
}


// pooleliii
async function loadTripAccomodations(tripID) {
    let trip = await fetchTrip(tripID);
    let tripAccomodationHtml = generateAccomodation(trip.accomodation);
    document.querySelector("#accomodationPanel-" + tripID).innerHTML = tripAccomodationHtml;
}


async function loadTripTransports(tripID) {
    let trip = await fetchTrip(tripID);
    let tripTransportHtml = generateTransport(trip.transport);
    document.querySelector("#transportPanel-" + tripID).innerHTML = tripTransportHtml;
}


async function loadTripChecklists(tripID) {
    let trip = await fetchTrip(tripID);
    let tripChecklistHtml = generateChecklists(trip.checklists);
    document.querySelector("#checklistPanel-" + tripID).innerHTML = tripChecklistHtml;
}

async function loadTripTasks(tripID) {
    let trip = await fetchTrip(tripID);
    let tripTaskHtml = generateTasks(trip.tasks);
    document.querySelector("#taskPanel-" + tripID).innerHTML = tripTaskHtml;
}

async function loadTripTravelDocuments(tripID) {
    let trip = await fetchTrip(tripID);
    let tripTravelDocumentHtml = generateTravelDocuments(trip.travelDocuments);
    document.querySelector("#travelDocumentPanel-" + tripID).innerHTML = tripTravelDocumentHtml;
}


function generateChecklists(checklists) {
    let checklistsHtml = "";

    for (let i = 0; i < checklists.length; i++) {
        checklistsHtml = checklistsHtml + `
        
            <div class="checkbox">
                <input type="checkbox" name="packersOff" value="1"/>
                 <label class="strikethrough" style="font-size: 20px;
                 color: #495057;">${checklists[i].listItem}</label> </button>
                 <button class="btn btn-outline-danger" onClick="removeChecklistItem(${checklists[i].id}, ${checklists[i].tripID})"><i class="material-icons">
                 clear
                 </i></button>               
                 </div>

        `;
    }
    console.log(checklistsHtml);
    return checklistsHtml;
}

function generateTasks(tasks) {
    let tasksHtml = "";

    for (let i = 0; i < tasks.length; i++) {
        tasksHtml = tasksHtml + `
            <div id="taskItem" style="font-size: 20px;
            color: #495057;">${tasks[i].taskItem} 
            <button class="btn btn-outline-danger" onClick="removeTask(${tasks[i].id}, ${tasks[i].tripID})"><i class="material-icons">
            clear
            </i></button></div>
        `;
    }
    console.log(tasksHtml);
    return tasksHtml;
}

function generateTravelDocuments(travelDocuments) {
    let travelDocumentsHtml = "";

    for (let i = 0; i < travelDocuments.length; i++) {
        travelDocumentsHtml = travelDocumentsHtml + `
        
            <div id="travelDocumentName">${travelDocuments[i].travelDocumentName} 
            <button  class="btn btn-outline-secondary" onClick="openTravelDocumentModalForUpdate(${travelDocuments[i].id})">
            <i class="material-icons">
            edit
            </i></button>
            <button class="btn btn-outline-danger" onClick="removeTravelDocument(${travelDocuments[i].id}, ${travelDocuments[i].tripID})"><i class="material-icons">
            clear
            </i></button>
            <button  class="btn btn-outline-secondary"><i class="material-icons" style="font-size: 24px; vertical-align: -20%;">
            attach_file
            </i></button></div>          
            <div id="travelDocumentStartTime">Valid:${travelDocuments[i].travelDocumentStartTime} to ${travelDocuments[i].travelDocumentEndTime}</div>
            <div id="travelDocumentExtraInfo">${travelDocuments[i].travelDocumentExtraInfo}</div>
            
            

            
        `;
    }
    console.log(travelDocumentsHtml);
    return travelDocumentsHtml;
}




function generateTripRowElement(cell3Content, cell4Content, cell5Content, cell6Content, cell7Content, cell8Content) {
    return /*html*/`

    <div class="col-md-12"  style="">
        <div class="row justify-content-right" >


       
     
        ${generateTripRowValueCell(cell4Content)}
        ${generateTripRowValueCell(cell6Content)}
        ${generateTripRowValueCell(cell8Content)}
       
       
            </div>
        </div>


    `;
}


function generateTripRowValueCell(text) {
    return /*html*/`
        <div class="col-sm-3" style="padding: 5px;">
            <div style="margin: 2px; padding: 10px; text-align: center;">
                ${text}
            </div>
        </div>
    `;
}

function generateTripRowButtonDelete(trip) {
    return /*html*/`
        
                <button class="btn btn-outline-danger" onClick="removeTrip(${trip.tripID})"><i class="material-icons">
                clear
                </i></button>
            
    `;
}

function generateTripRowButtonEdit(trip) {
    return /*html*/`
                <button  class="btn btn-outline-secondary"onClick="openTripModalForUpdate(${trip.tripID})">
                <i class="material-icons">
                edit
                </i></button>
            
       
    `;
}

function generateImageElement(logo) {
    if (!isEmpty(logo)) {
        return /*html*/`<img src="${logo}" width="150" />`;
    } else {
        return "[PILT PUUDUB]";
    }
}

function openTripModal() {
    $("#tripModal").modal('show');
}

function closeTripModal() {
    $("#tripModal").modal('hide');
}

function openAccomodationModal() {
    $("#accomodationModal").modal('show');
}

function closeAccomodationModal() {
    $("#accomodationModal").modal('hide');
}

function openTransportModal() {
    $("#transportModal").modal('show');
}

function closeTransportModal() {
    $("#transportModal").modal('hide');
}

function openChecklistModal() {
    $("#checklistModal").modal('show');
}

function closeChecklistModal() {
    $("#checklistModal").modal('hide');
}

function openTaskModal() {
    $("#taskModal").modal('show');
}

function closeTaskModal() {
    $("#taskModal").modal('hide');
}

function openTravelDocumentModal() {
    $("#travelDocumentModal").modal('show');
}

function closeTravelDocumentModal() {
    $("#travelDocumentModal").modal('hide');
}

function openRegistrationModal() {
    $("#modalRegisterForm").modal('show');
}

function closeRegistrationModal() {
    $("#modalRegisterForm").modal('hide');
}

function clearError() {
    document.getElementById("errorPanel").innerHTML = "";
    document.getElementById("errorPanel").style.display = "none";
}

function showError(message) {
    document.getElementById("errorPanel").innerHTML = message;
    document.getElementById("errorPanel").style.display = "block";
}

function clearTripModal() {
    document.getElementById("tripLocation").value = null;
    document.getElementById("tripType").value = null;
    document.getElementById("tripStartTime").value = null;
    document.getElementById("tripEndTime").value = null;
    document.getElementById("tripID").value = null;
}

function clearAccomodationModal() {

    document.getElementById("accomodationId").value = null;
    document.getElementById("accomodationModalName").value = null;
    document.getElementById("accomodationModalStartTime").value = null;
    document.getElementById("accomodationModalEndTime").value = null;
    document.getElementById("accomodationModalAddress").value = null;
    document.getElementById("accomodationModalPhoneNo").value = null;
    document.getElementById("accomodationModalType").value = null;
    document.getElementById("accomodationModalExtraInfo").value = null;
}

function clearTransportModal() {

    document.getElementById("transportId").value = null;
    document.getElementById("transportModalProvider").value = null;
    document.getElementById("transportModalStartTime").value = null;
    document.getElementById("transportModalEndTime").value = null;
    document.getElementById("transportModalStartLocation").value = null;
    document.getElementById("transportModalEndLocation").value = null;
    document.getElementById("transportModalType").value = null;

}

function clearChecklistModal() {

    document.getElementById("checklistId").value = null;
    document.getElementById("checklistModalItem").value = null;

}

function clearTaskModal() {
    document.getElementById("taskId").value = null;
    document.getElementById("taskModalItem").value = null;

}

function clearTravelDocumentModal() {

    document.getElementById("travelDocumentId").value = null;
    document.getElementById("travelDocumentModalName").value = null;
    document.getElementById("travelDocumentModalStartTime").value = null;
    document.getElementById("travelDocumentModalEndTime").value = null;
    document.getElementById("travelDocumentModalExtraInfo").value = null;
}


function fillTripModal(trip) {

    document.getElementById("tripID").value = trip.tripID;
    document.getElementById("tripLocation").value = trip.tripLocation;
    document.getElementById("tripType").value = trip.tripType;
    document.getElementById("tripStartTime").value = trip.tripStartTime;
    document.getElementById("tripEndTime").value = trip.tripEndTime;

}

function fillAccomodationModal(accomodation) {

    document.getElementById("accomodationId").value = accomodation.id;
    document.getElementById("accomodationTripId").value = accomodation.tripID;
    document.getElementById("accomodationModalName").value = accomodation.accomodationName;
    document.getElementById("accomodationModalStartTime").value = accomodation.accomodationStartTime;
    document.getElementById("accomodationModalEndTime").value = accomodation.accomodationEndTime;
    document.getElementById("accomodationModalAddress").value = accomodation.accomodationAddress;
    document.getElementById("accomodationModalPhoneNo").value = accomodation.accomodationPhoneNo;
    document.getElementById("accomodationModalType").value = accomodation.accomodationType;
    document.getElementById("accomodationModalExtraInfo").value = accomodation.accomodationExtraInfo;
}

function fillTransportModal(transport) {

    document.getElementById("transportId").value = transport.id;
    document.getElementById("transportTripId").value = transport.tripID;
    document.getElementById("transportModalProvider").value = transport.transportProvider;
    document.getElementById("transportModalStartTime").value = transport.transportStartTime;
    document.getElementById("transportModalEndTime").value = transport.transportEndTime;
    document.getElementById("transportModalStartLocation").value = transport.transportStartLocation;
    document.getElementById("transportModalEndLocation").value = transport.transportEndLocation;
    document.getElementById("transportModalType").value = transport.transportType;
}

function fillTravelDocumentModal(travelDocument) {

    document.getElementById("travelDocumentId").value = travelDocument.id;
    document.getElementById("travelDocumentTripId").value = travelDocument.tripID;
    document.getElementById("travelDocumentModalName").value = travelDocument.travelDocumentName;
    document.getElementById("travelDocumentModalStartTime").value = travelDocument.travelDocumentStartTime;
    document.getElementById("travelDocumentModalEndTime").value = travelDocument.travelDocumentEndTime;
    document.getElementById("travelDocumentModalExtraInfo").value = travelDocument.travelDocumentExtraInfo;
}

function fillCompanyModalLogoField(logo) {
    document.getElementById("logo").value = logo;
}
