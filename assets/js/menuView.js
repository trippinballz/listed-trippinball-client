
function generateTopMenu() {
    let logoutLink = "";
    if (!isEmpty(getUsername())) {
        logoutLink = /*html*/`${getUsername()} | <a href="javascript:logoutUser()" style="color: #6B5C4F; font-weight: bold;">Log out</a>`;
    }

    document.getElementById("topMenuContainer").innerHTML = /*html*/`
        <div style="padding: 5px; color: #6B5C4F; font-style: italic; background:maroon, opacity: 0%;">
            <div class="row">
                <div class="col-6">
                    <strong>Vali IT project TrippinBall</strong>
                </div>
                <div class="col-6" style="text-align: right; font-style: normal; color:  #6B5C4F">
                ${logoutLink}
                </div>
            </div>
        </div>
    `;
}

function showLoginContainer() {
    document.getElementById("loginContainer").style.display = "block";
    document.getElementById("mainContainer").style.display = "none";
}

function showMainContainer() {
    document.getElementById("loginContainer").style.display = "none";
    document.getElementById("mainContainer").style.display = "block";
}

function changeView() {
    if (document.getElementById("viewSelect").value === "CHART") {
        showCompanyChart();
    } else {
        showTripList();
    }
}

function showTripList() {
    document.getElementById("tripList").style.display = "block";
    document.getElementById("companyChart").style.display = "none";
    loadTrips();
}

function showCompanyChart() {
    document.getElementById("companyList").style.display = "none";
    document.getElementById("companyChart").style.display = "block";
    loadChart();
}
