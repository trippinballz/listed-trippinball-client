
function fetchTrips() {
    return fetch(
        `${API_URL}/trips`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse)
    .then(trips => trips.json());
}

function fetchTrip(tripID) {
    return fetch(
        `${API_URL}/trips/${tripID}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(trip => trip.json());
}


function fetchAccomodation(id) {
    return fetch(
        `${API_URL}/trips/accomodation/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(accomodation => accomodation.json());
}

function fetchTransport(id) {
    return fetch(
        `${API_URL}/trips/transport/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(transport => transport.json());
}

// function fetchTransport() {
//     return fetch(
//         `${API_URL}/trips/transport`,
//         {
//             method: 'GET',
//             headers: {
//                 'Authorization': `Bearer ${getToken()}`
//             }
//         }
//     )
//     .then(checkResponse).then(transport => transport.json());
// }

function fetchChecklist(id) {
    return fetch(
        `${API_URL}/trips/checklist/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(checklist => checklist.json());
}

function fetchTask(id) {
    return fetch(
        `${API_URL}/trips/task/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(task => task.json());
}

function fetchTravelDocument(id) {
    return fetch(
        `${API_URL}/trips/travelDocument/${id}`,
        {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse).then(travelDocument => travelDocument.json());
}





function uploadFile(file) {
    let formData = new FormData();
    formData.append("file", file);

    return fetch(
            `${API_URL}/files/upload`, 
            {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${getToken()}`
                },
                body: formData
            }
    )
    .then(checkResponse).then(response => response.json());
}

function postTrip(trip) {
    return fetch(
        `${API_URL}/trips/trip`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(trip)
        }
    )
    .then(checkResponse);
}

// Post fetches

function postAccomodation(accomodation) {
    return fetch(
        `${API_URL}/trips/accomodation`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(accomodation)
        }
    )
    .then(checkResponse);
}

function postTransport(transport) {
    return fetch(
        `${API_URL}/trips/transport`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(transport)
        }
    )
    .then(checkResponse);
}

function postChecklist(checklist) {
    return fetch(
        `${API_URL}/trips/checklist`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(checklist)
        }
    )
    .then(checkResponse);
}

function postTask(task) {
    return fetch(
        `${API_URL}/trips/task`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(task)
        }
    )
    .then(checkResponse);
}

function postTravelDocument(travelDocument) {
    return fetch(
        `${API_URL}/trips/travelDocument`, 
        {
            method: 'POST', 
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${getToken()}`
            },
            body: JSON.stringify(travelDocument)
        }
    )
    .then(checkResponse);
}

// Apis for delete (ei tööta?)


function deleteTrip(id) {
    return fetch(
        `${API_URL}/trips/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function deleteAccomodation(id) {
    return fetch(
        `${API_URL}/trips/accomodation/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function deleteChecklistItem(id) {
    return fetch(
        `${API_URL}/trips/checklist/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function deleteTask(id) {
    return fetch(
        `${API_URL}/trips/task/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function deleteTransport(id) {
    return fetch(
        `${API_URL}/trips/transport/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}

function deleteTravelDocument(id) {
    return fetch(
        `${API_URL}/trips/travelDocument/${id}`,
        {
            method: 'DELETE',
            headers: {
                'Authorization': `Bearer ${getToken()}`
            }
        }
    )
    .then(checkResponse);
}



async function login(credentials) {
    return fetch(
        `${API_URL}/users/login`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    )
    .then(checkResponse)
    .then(session => session.json());
}

async function register(credentials) {
    let response = await fetch(
        `${API_URL}/users/register`,
        {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(credentials)
        }
    );
    checkResponse(response);
    return await response.json();
}

function checkResponse(response) {
    if (!response.ok) {
        clearAuthentication();
        showLoginContainer();
        // closeCompanyModal();
        closeRegistrationModal();
        generateTopMenu();
        throw new Error(response.status);
    }
    showMainContainer();
    return response;
}
