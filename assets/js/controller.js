let allTrips = [];

function loadTrips() {
    fetchTrips().then(
        function (trips) {
            allTrips = trips;
            generateTripList(allTrips);
        }
    );
}

function openTripModalForUpdate(tripID) {
    //    clearError();
    fetchTrip(tripID).then(trip => {
        openTripModal();

        clearTripModal();
        fillTripModal(trip);
    });
}

function openAccomodationModalForUpdate(id) {
    //    clearError();
    fetchAccomodation(id).then(accomodation => {
        openAccomodationModal();

        clearAccomodationModal();
        fillAccomodationModal(accomodation);
    });
}

function openTravelDocumentModalForUpdate(id) {
    //    clearError();
    fetchTravelDocument(id).then(travelDocument => {
        openTravelDocumentModal();

        clearTravelDocumentModal();
        fillTravelDocumentModal(travelDocument);
    });
}

function openTransportModalForUpdate(id) {
    //    clearError();
    fetchTransport(id).then(transport => {
        openTransportModal();

        clearTransportModal();
        fillTransportModal(transport);
    });
}



function openTripModalForInsert() {
    openTripModal();
    clearError();
    clearTripModal();
}

function openAccomodationModalForInsert(tripId) {
    document.querySelector("#accomodationTripId").value = tripId;
    // document.querySelector("#accomodationId").value = id;
    openAccomodationModal();
    clearError();
    clearAccomodationModal();
}

function openTransportModalForInsert(tripId) {
    document.querySelector("#transportTripId").value = tripId;
    openTransportModal();
    clearError();
    clearTransportModal();
}

function openChecklistModalForInsert(tripId) {
    document.querySelector("#checklistTripId").value = tripId;
    openChecklistModal();
    clearError();
    clearChecklistModal();
}

function openTaskModalForInsert(tripId) {
    document.querySelector("#taskTripId").value = tripId;
    openTaskModal();
    clearError();
    clearTaskModal();
}

function openTravelDocumentModalForInsert(tripId) {
    document.querySelector("#travelDocumentTripId").value = tripId;
    openTravelDocumentModal();
    clearError();
    clearTravelDocumentModal();
}

function saveFile(event) {
    event.preventDefault();
    let fileInput = getFileInput();
    uploadFile(fileInput.files[0]).then(response => fillCompanyModalLogoField(response.url));
}

function saveTrip() {
    let trip = getTripFromModal();
    // if (validateTripModal(trip)) {
    postTrip(trip)
        .then(() => {
            closeTripModal();
            loadTrips();
        });
    // }
}

function saveAccomodation() {
    let accomodation = getAccomodationFromModal();
    // if (validateTripModal(trip)) {
    postAccomodation(accomodation)
        .then(() => {
            closeAccomodationModal();
            loadTrips();
        });
    // }
}

function saveTransport() {
    let transport = getTransportFromModal();
    // if (validateTripModal(trip)) {
    postTransport(transport)
        .then(() => {
            closeTransportModal();
            // loadTrips();
            loadTripTransports(transport.tripID);
        });
    // }
}

function saveChecklist() {
    let checklist = getChecklistFromModal();
    // if (validateTripModal(trip)) {
    postChecklist(checklist)
        .then(() => {
            closeChecklistModal();
            loadTripChecklists(checklist.tripID);
             });
    // }
}

function saveTask() {
    let task = getTaskFromModal();
    // if (validateTripModal(trip)) {
    postTask(task)
        .then(() => {
            closeTaskModal();
            loadTripTasks(task.tripID);
        });
    // }
}

function saveTravelDocument() {
    let travelDocument = getTravelDocumentFromModal();
    // if (validateTripModal(trip)) {
    postTravelDocument(travelDocument)
        .then(() => {
            closeTravelDocumentModal();
            // loadTrips();
            loadTripTravelDocuments(travelDocument.tripID);
        });
    // }
}


// remove controllers

function removeTrip(id) {

    if (confirm('You sure about that? All the memories...')) {
        deleteTrip(id).then(loadTrips)
    }
}

function removeAccomodation(id) {
    document.querySelector("#accomodationId").value = id;
    if (confirm('You sure about that?')) {
        deleteAccomodation(id).then(loadTrips)
    }
}

async function removeChecklistItem(id, tripID) {
    document.querySelector("#checklistId").value = id;
    await deleteChecklistItem(id)
    loadTripChecklists(tripID);
}

async function removeTask(id, tripID) {
    document.querySelector("#taskId").value = id;
    await deleteTask(id)
    loadTripTasks(tripID);
}

async function removeTransport(id, tripID) {
    document.querySelector("#transportId").value = id;
    if (confirm('You sure about that?')) {
        await deleteTransport(id)
        loadTripTransports(tripID);
    }
}

async function removeTravelDocument(id, tripID) {
    document.querySelector("#travelDocumentId").value = id;
    if (confirm('You sure about that?')) {
        await deleteTravelDocument(id)
        loadTripTravelDocuments(tripID);
    }
}


// function loadChart() {
//     fetchCompanies().then(generateCompanyChart)
// }

function loginUser() {
    let credentials = getCredentialsFromLoginContainer();
    if (validateCredentials(credentials)) {
        login(credentials).then(session => {
            storeAuthentication(session);
            generateTopMenu();
            loadTrips();
        })
    }
}


// function registerUser() {
//     let credentials = getCredentialsFromRegisterContainer();
//     if (validateCredentials(credentials)) {
//         register(credentials).then(session => {
//         storeAuthentication(session);
//         generateTopMenu();
//         closeRegistrationModal();
//         showMainContainer();
//         loadTrips();
//         })
// }
// }




async function registerUser() {
    let credentials = getCredentialsFromRegisterContainer();
    if (validateCredentials(credentials)) {
        await register(credentials);
        let session = await login(credentials);
        storeAuthentication(session);
        generateTopMenu();
        showMainContainer();
        loadTrips();
        closeRegistrationModal();

    }
}


function logoutUser() {
    clearAuthentication();
    generateTopMenu();
    showLoginContainer();
}
